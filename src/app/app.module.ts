import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatFormFieldModule} from '@angular/material/form-field';
import {TrendsComponent} from './trends/trends.component';
import {HttpClientModule} from '@angular/common/http';
import {MatInputModule} from '@angular/material/input';
import {MatMenuModule} from '@angular/material/menu';
import {TweetsFromHashtagComponent} from './tweets-from-hashtag/tweets-from-hashtag.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatCardModule} from '@angular/material/card';
import {MapsComponent} from './maps/maps.component';
import {GoogleMapsModule} from '@angular/google-maps';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {MatGridListModule} from '@angular/material/grid-list';
import {AgmCoreModule} from '@agm/core';
import {TweetsFromLocationComponent} from './tweets-from-location/tweets-from-location.component';


@NgModule({
  declarations: [
    AppComponent,
    TrendsComponent,
    TweetsFromHashtagComponent,
    MapsComponent,
    TweetsFromLocationComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    HttpClientModule,
    MatFormFieldModule,
    MatInputModule,
    MatMenuModule,
    ReactiveFormsModule,
    MatProgressSpinnerModule,
    MatCardModule,
    GoogleMapsModule,
    FontAwesomeModule,
    MatGridListModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyChUYEhKW833sWyd_pA8LE4A8O17CkXv-0',
      libraries: ['places']
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
