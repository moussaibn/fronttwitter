import {Component, OnInit} from '@angular/core';
import {ApiService} from '../api.service';
import {FormBuilder} from '@angular/forms';

@Component({
  selector: 'app-trends',
  templateUrl: './trends.component.html',
  styleUrls: ['./trends.component.scss']
})
export class TrendsComponent implements OnInit {
  trends;
  checkoutForm;


  constructor(private apiService: ApiService,
              private formBuilder: FormBuilder) {
    this.checkoutForm = this.formBuilder.group({
      lieu: '',
    });

  }

  ngOnInit(): void {
    this.apiService.getTrends('france').subscribe((data) => {
      console.log(data);
      this.trends = data;
    });
  }

  onSubmit(customerData) {
    // Process checkout data here
    this.apiService.getTrends(customerData.lieu).subscribe((data) => {
      console.log(data);
      this.trends = data;
    });
    console.warn('Your order has been submitted', customerData);

    this.checkoutForm.reset();
  }
}
