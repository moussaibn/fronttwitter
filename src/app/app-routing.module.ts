import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {TrendsComponent} from './trends/trends.component';
import {TweetsFromHashtagComponent} from './tweets-from-hashtag/tweets-from-hashtag.component';
import {TweetsFromLocationComponent} from './tweets-from-location/tweets-from-location.component';


const routes: Routes = [

  {path: '', component: TrendsComponent},
  {path: 'tweetsFromHashtag' , component: TweetsFromHashtagComponent},
  {path: 'tweetFromLocation', component: TweetsFromLocationComponent},
  {path: 'tweetsFromHashtag/:hashtag' , component: TweetsFromHashtagComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
