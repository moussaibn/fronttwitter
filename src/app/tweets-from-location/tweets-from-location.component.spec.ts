import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TweetsFromLocationComponent } from './tweets-from-location.component';

describe('TweetsFromLocationComponent', () => {
  let component: TweetsFromLocationComponent;
  let fixture: ComponentFixture<TweetsFromLocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TweetsFromLocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TweetsFromLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
