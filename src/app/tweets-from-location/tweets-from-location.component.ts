import {Component, OnInit} from '@angular/core';
import {ApiService} from '../api.service';

@Component({
  selector: 'app-tweets-from-location',
  templateUrl: './tweets-from-location.component.html',
  styleUrls: ['./tweets-from-location.component.scss']
})
export class TweetsFromLocationComponent implements OnInit {


  tweets;

  constructor(private apiService: ApiService) {

  }

  latitude: number;
  longitude: number;
  zoom: number;
  isFinished: boolean;
  coordinates: string;


  ngOnInit() {
    this.setCurrentLocation();
    this.isFinished = false;
  }

  // Get Current Location Coordinates
  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 5;
      });
    }
  }


  markerDragEnd($event) {

    console.log($event);
    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    this.coordinates = '' + this.latitude + ',' + this.longitude;
    console.log(this.coordinates);
    this.apiService.getTweetsFromLocation(this.coordinates).subscribe((data) => {
      console.log(data);
      this.tweets = data;
      for (let tweet of this.tweets) {
        tweet.addresse.location.lat += (Math.random() - 0.5)/10;
        tweet.addresse.location.lng += (Math.random() - 0.5)/10;
      }
      this.isFinished = true;
    });
  }
}

