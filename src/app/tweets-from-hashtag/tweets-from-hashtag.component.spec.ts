import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TweetsFromHashtagComponent } from './tweets-from-hashtag.component';

describe('TweetsFromHashtagComponent', () => {
  let component: TweetsFromHashtagComponent;
  let fixture: ComponentFixture<TweetsFromHashtagComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TweetsFromHashtagComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TweetsFromHashtagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
