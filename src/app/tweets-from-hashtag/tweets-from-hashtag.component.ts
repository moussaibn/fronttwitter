import {Component, OnInit} from '@angular/core';
import {ApiService} from '../api.service';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder} from '@angular/forms';

@Component({
  selector: 'app-tweets-from-hashtag',
  templateUrl: './tweets-from-hashtag.component.html',
  styleUrls: ['./tweets-from-hashtag.component.scss']
})
export class TweetsFromHashtagComponent implements OnInit {
  tweets;
  hashtag;
  checkoutForm;
  reload: boolean;

  constructor(private apiService: ApiService,
              private route: ActivatedRoute,
              private formBuilder: FormBuilder) {
    this.checkoutForm = this.formBuilder.group({
      searchHashtag: '',
    });
  }

  ngOnInit() {
  }

  onSubmit(customerData) {
    // Process checkout data here
    this.hashtag = customerData.searchHashtag;
    this.apiService.getTweetsFromHashtag(customerData.searchHashtag).subscribe((data) => {
      console.log(data);
      console.log(this.reload);
      this.tweets = data;
    });
    console.warn('Your order has been submitted', customerData);

    this.checkoutForm.reset();
    this.reload = true;
  }
}
