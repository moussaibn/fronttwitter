import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpClient: HttpClient) {
  }

  public getTrends(lieu) {
    return this.httpClient.get('http://localhost:8080/getTrends/' + lieu);
  }
  public getTweetsFromHashtag(hashtag) {
    return this.httpClient.get('http://localhost:8080/getTweets/' + hashtag);
  }
  public getTweetsFromLocation(latlng) {
    return this.httpClient.get('http://localhost:8080/getTweets/coordinates/' + latlng);
  }

}
