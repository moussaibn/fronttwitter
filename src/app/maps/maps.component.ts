import {Component, Input, OnInit, DoCheck} from '@angular/core';

@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.scss']
})
export class MapsComponent implements OnInit, DoCheck {
  zoom = 2;
  center: google.maps.LatLngLiteral;
  options: google.maps.MapOptions = {
    mapTypeId: 'roadmap',
    zoomControl: true,
    scrollwheel: true,
    draggable: true,
    disableDoubleClickZoom: true,
    maxZoom: 15,
    minZoom: 1,

  };
  @Input() getTweets: [];
  markers = [];
  infoContent: any;
  oldGetTweets: [];
  @Input() isReloaded;

  constructor() {
  }

  ngOnInit() {

    navigator.geolocation.getCurrentPosition(position => {
      this.center = {
        lat: position.coords.latitude,
        lng: position.coords.longitude,
      };
    });
    this.oldGetTweets = this.getTweets;

    for (const tweet of this.getTweets) {
      console.log(tweet);
      this.addMarker(tweet);
    }
  }

  selectMarker(event) {
    console.log(event);
    this.infoContent = event.tweet;
  }

  ngDoCheck() {
    if (this.isReloaded) {
      /*if (this.markers.length > 0) {
        for (let m of this.markers) {
          m.setMap(null);
        }
      }*/
      this.isReloaded = false;
      this.markers = [];
      this.ngOnInit();
    }
  }

  addMarker(t) {
    this.markers.push({
      position: {
        lat: t.addresse.location.lat,
        lng: t.addresse.location.lng,
      },
      label: {
        color: 'red',
        text: t.text,
        screenName: t.screenName,
      },
      title: t.screenName,
      options: {animation: google.maps.Animation.BOUNCE},
      tweet: t,
    });
  }

}
